# Receipt Scanner using GCV

Using Google Cloud Vision API for multi-lingual receipt scanner.

</br>

## **Algorithm Steps:**
***

- Read image
- Pre-processing
- Run GCV text recognition
- Pre-processing pipeline [deprecated]
- Post-processing
- Save JSON output

</br>

## **Input:**
***

- Image
- /path/to/image

</br>


## **Output:**
***

- JSON object of items and prices

**Example:**

```
{
  "items": {
    "M Cheese": 68.0,
    "Chkn Crispy Ranch": 75.0,
    "Soft": 1.0,
    "Refill": 2.0,
    "Crispy Chicken H": 72.0,
    "Service": 12.0,
    "Vat": 48.3,
    "Total Due": 393.26
  },
  "total": 393.26
}
```

</br>

## **Steps to run algorithm:**
***
 - ### *Install the GCP SDK*
```shell
sh ./install_gcp_sdk.sh
```

 - ### *Put the JSON key file in the `key` directory*


 - ### *Run the algorithm*
   - Run the server
```python
python3 main_server.py
```
   - Run the client
```python
python3 main_client.py --img_path </path/to/image.jpg>
```
