echo "[INFO] Downloading the SDK"
curl -O https://dl.google.com/dl/cloudsdk/channels/rapid/downloads/google-cloud-sdk-326.0.0-linux-x86_64.tar.gz

echo "[INFO] Extracting the SDK"
tar -zxvf google-cloud-sdk-326.0.0-linux-x86_64.tar.gz 

echo "[INFO] Installing the SDK"
./google-cloud-sdk/install.sh

echo "[INFO] Initializing the SDK"
./google-cloud-sdk/bin/gcloud init

echo "[INFO] Installing the required libraries"
pip install -r requirements.txt

echo "[INFO] Checking for key"
if [ ! -f  "$PWD/key/ReceiptScanner-1b921636543e.json" ]; then
    echo "[ERROR] Key not found!"
    echo "Please make sure to put your JSON key in the 'key' directory"
    exit 1
else
    echo "[INFO] Key found!"
fi

echo "[INFO] Adding Google Application Credentials to PATH"
export GOOGLE_APPLICATION_CREDENTIALS="$PWD/key/ReceiptScanner-1b921636543e.json"

echo "DONE!"
