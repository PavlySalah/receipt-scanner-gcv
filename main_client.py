'''
Send image JSON to server and get respose
'''

import argparse
import base64
import json
import requests


# Define the --img_path argument
parser = argparse.ArgumentParser()
parser.add_argument('--img_path', '-p', help='Path to input image',\
    type=str, required=True)

# Get image path from user
args = parser.parse_args()
image_path = args.img_path

print(f'[DEBUG] Image path: {image_path}')

# Server address
addr = 'http://localhost:5000'

# Headers for http request
content_type = 'application/json'
headers = {'content-type': content_type}

# Convert image to byts
with open(image_path, "rb") as iff:
    img_bytes = base64.b64encode(iff.read())

# Convert image to string
# because JSON objects don't accept bytes
img_str = img_bytes.decode()

# Create JSON object
json_request = json.dumps({"Image": img_str})

# Send request and receive response
response = requests.post(addr, data=json_request, headers=headers)

# Decode response
print(f'[INFO] Response:\n{response.text}')
