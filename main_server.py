'''
Main server
'''
import os
import base64
import json
from flask import Flask, request, jsonify
from src.pipeline import pipeline


def export_key():
    '''Export the GOOGLE_APPLICATION_CREDENTIALS key'''

    # Key path
    KEY_PATH = '/path/to/key.json'

    # Make sure the key exists
    if not os.path.exists(KEY_PATH):
        raise FileNotFoundError('You do not have a GCP key file!')

    # Export the key if it isn't in $PATH already
    try:
        _ = os.environ["GOOGLE_APPLICATION_CREDENTIALS"]
        print(f'[INFO] Key Found!')
    except KeyError:
        # Export the key
        print(f'[DEBUG] Exporting key')
        os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = KEY_PATH


# Initialize the Flask application
app = Flask(__name__)


# Route http posts to this method
@app.route('/', methods=['POST'])
def mainy():
    '''Test receive image'''
    # Load JSON object
    req = json.loads(request.data)

    # Get the string iage and encde it to bytes
    req_img = req['Image'].encode()

    # Save the image in a specified path
    image_path = 'image_out.jpg'
    with open(image_path, 'wb') as off:
        off.write(base64.decodebytes(req_img))

    # Get the items and prices from src/pipeline
    items_prices_dict = pipeline(image_path)

    # Build a response dict to send back to client
    response = jsonify(items_prices_dict)
    return response


if __name__ == '__main__':
    export_key()

    # Start flask app
    app.run(host="0.0.0.0", port=5000, debug=True)
