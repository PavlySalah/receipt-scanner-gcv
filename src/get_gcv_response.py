'''Get text and boudning boxes from GCV'''
import io
import cv2
from google.cloud import vision
import sys

class GCV():
    '''GCV Response (text and boxes)'''
    def __init__(self):
        '''Initalise GCV client'''
        self.client = vision.ImageAnnotatorClient()
        self.text_box_dict = {}
        self.temp_path = './data/temp.jpg'


    def _convert_img_to_bytes(self, img):
        '''
        Convert CV2 image to bytes

        Args:
            img (ndarray): image read by CV2

        Returns:
            img_bytes (bytes): image in bytes
        '''
        img_bytes = cv2.imencode('.jpg', img)[1].tobytes()
        return img_bytes


    def _save_img(self, img):
        '''
        Save image to temp path

        Args:
            img (ndarray): image read by CV2
        '''
        cv2.imwrite(self.temp_path, img)


    def _read_img_bytes(self, path):
        '''
        Read image and convert it into bytes.

        Args:
            path (str): path to image

        Returns:
            content (bytes): image bytes
        '''
        with io.open(path, 'rb') as image_file:
            content = image_file.read()

        return content


    def _detect_text(self, img_bytes):
        '''
        Detects text in the file. and return the text with its bounding box

        Args:
            img_bytes (str): image in bytes

        Returns:
            text_box_dict (dict): dictionary of the text and
                                its respective bounding box
        '''
        # Send image to GCV as a request
        image = vision.Image(content=img_bytes)

        # Run GCV OCR ###OLD###
        response = self.client.text_detection(image=image)
        gcv_texts = response.text_annotations

        ### NEW ###
        # response = self.client.document_text_detection(image=image)
        # gcv_texts = response.full_text_annotation

        # Raise an error if GCP returned an error message
        if response.error.message:
            raise Exception(
                '{}\nFor more info on error messages, check: '
                'https://cloud.google.com/apis/design/errors'.format(
                    response.error.message))

        return gcv_texts


    def recognise_text(self, img):
        '''
        Main pipeline.

        Args:
            img (ndarray): CV2 image

        Returns:
            text_box_dict (dict): dict of text and boudning boxes
        '''
        # self._save_img(img)
        # img_bytes = self._read_img_bytes(self.temp_path)
        img_bytes = self._convert_img_to_bytes(img)
        text = self._detect_text(img_bytes)
        # print(f'[DEBUG] text: \n{text}')
        return text


def main():
    '''main'''
    import flask

    img_path = './data/ME/3.jpg'
    gcv = GCV()

    img = cv2.imread(img_path)
    response = gcv.recognise_text(img)

    json_out = flask.jsonify({"respnse": response})


    with open('test_gcv_out.json', 'w') as off:
        json.dump(json_out, off, ensure_ascii=False)


if __name__ == '__main__':
    import json
    main()
