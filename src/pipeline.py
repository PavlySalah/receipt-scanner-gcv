'''
Read image
Pre-process
Recognise text
Post-process
Save items_prices_dict
'''

import cv2
from src.get_gcv_response import GCV
from src.post_process import PostProc


def pipeline(img_path):
    '''
    Run the pipeline
        * Read
        * Pre-process
        * Text recognition
        * Post-processing
        * Save output

    Args:
        img_path (str): path to image

    Returns:
        items_prices_dict (dict): dict of items and prices
    '''
    print("[DEBUG] Reading image...")
    img = cv2.imread(img_path)
    Posty = PostProc(img.shape[0])
    gcv = GCV()

    print("[DEBUG] Recognising text...")
    text_box_dict = gcv.recognise_text(img)

    print('[DEBUG] Post-processing..')
    items_prices_dict = Posty.post_process(text_box_dict)

    return items_prices_dict


def pipeline_img(img):
    '''
    Run the pipeline
        * Pre-process
        * Text recognition
        * Post-processing
        * Save output

    Args:
        img (ndarray): input image read by CV2

    Returns:
        items_prices_dict (dict): dict of items and prices
    '''
    print(f'[DEBUG] Pipeline started')
    Posty = PostProc(img.shape[0])
    gcv = GCV()

    print("[DEBUG] Recognising text...")
    text_box_dict = gcv.recognise_text(img)

    print('[DEBUG] Post-processing..')
    items_prices_dict = Posty.post_process(text_box_dict)

    return items_prices_dict


def main():
    '''main'''
    img_path = './data/New/8.png'
    out = pipeline(img_path)
    print(f'[DEBUG] Items and prices dict:\n', out)


if __name__ == '__main__':
    main()
