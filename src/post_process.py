'''
Post processing for GCV output.
'''

import json
import re

GCV_FUNCTION = False

if GCV_FUNCTION:
    from flask import jsonify


# TODO: WER for total checking, slurs removal and skip_words


class PostProc:
    '''GCV post-processing'''

    def __init__(self, img_height):
        # Y-axis threshold to combine N bounding boxes
        self.Y_THRESH = img_height // 50
        self.items_prices_dict = {'items': {}, 'total': 0.00}
        self.new_dict = {}            # New formatted dict

        self.price_re = r'(^\d+\.\d*)|(\d+\.\d*$)|(\s\d+\.\d*\s)'
        self.item_re = r'[ء-يa-zA-Z*(\s)?]+'
        self.item_price_pattern = r'([ء-يA-Za-z*\s]+(\d+.\d*)+)|((\d+.\d*)+[ء-يA-Za-z*\s?]+)'

        # Read the total_keys and skip_words files
        with open('./data/key_matches/total_keys.txt', 'r') as iff:
            self.total_keys = list(map(lambda x: x.strip(), iff.readlines()))

        with open('./data/key_matches/skip_words.txt', 'r') as iff:
            self.skip_words = list(map(lambda x: x.strip(), iff.readlines()))

        # List of irrelevant characters thhat should be removed from the price
        self.char_pattern = r'[\[\]\|\(\)\{\}\,\"\~\'\`\-\$\*\@\!\#\_\=\^\&\%\:\;\“\?\>\<\\]'


    def _get_text_box(self, gcv_texts):
        '''
        Get text and its respective bounding box coordinate

        Args:
            gcv_texts (RepeatedComposite): response text from GCV

        Returns:
            text_box_dict (dict): dict of text and boudning boxes
        '''
        text_box_dict = {}

        # Populate the text_box_dict
        for text in gcv_texts: # index from 1 to discard the full text
            vertices = [(vertex.x, vertex.y) for vertex in text.bounding_poly.vertices]
            text_box_dict[text.description] = vertices

        return text_box_dict


    def _get_area(self, box):
        '''Get area of bounding box'''
        return (box[2][1] - box[0][1])*(box[2][0] - box[0][0])



    def _format_dict(self, txt_bx_dct):
        '''Flatten the bounding boxes for easier manipulation'''
        clean_dict = {}

        # Remove the box with the maximum area; it contains the entire text in the receipt
        max_area = max([self._get_area(box) for box in txt_bx_dct.values()])

        for txt, box in txt_bx_dct.items():
            if self._get_area(box) != max_area:
                clean_dict[txt] = box

        self.new_dict = clean_dict


    def _calculate_distance_matrix(self):
        '''Calculate distance matrix'''
        distance_matrix = []       # distance between each box and all the other boxes

        # Populate distance matrix using Y2
        for box1 in self.new_dict.values():
            distance_matrix.append([abs(box1[2][1] - box2[2][1])
                                    for box2 in self.new_dict.values()])

        return distance_matrix


    def _get_new_bounds(self, distance_matrix):
        '''Get the merged bounding boxes using the distance matrix'''
        new_bounds = []            # array of new -- merged -- bounding boxes

        # Iterate over the distance matrix and cobine boxes
        for i, distances in enumerate(distance_matrix):
            temp_merged_idx = []

            for j, dist in enumerate(distances):
                if dist < self.Y_THRESH and i != j:
                    temp_merged_idx.extend([i, j])

            new_bounds.append(list(set(temp_merged_idx)))

        return new_bounds


    def _clean_bounding_boxes(self, new_bounds):
        '''
        Clean the merged boxes, because I'm bad at programming

        If there are multiple boxes that share indices, keep the one with max length.

        E.g. If there are boxes `[[0, 1], [0, 2], [1, 2], [0, 1, 2]]`
        Kepp the `[0, 1, 2]` box only.
        '''
        clean_bounds = []

        # Clean the items that are 1 character long
        for x in new_bounds:
            if len(x) > 1 and x not in clean_bounds:
                clean_bounds.append(x)

        # Intersection, keep the one with max length
        for i, x in enumerate(clean_bounds):
            for j, y in enumerate(clean_bounds):
                if x is not y and set(x).intersection(set(y)):
                    if len(x) >= len(y):
                        del clean_bounds[j]
                    else:
                        del clean_bounds[i]

        return sorted(clean_bounds)


    def _merge_text(self, clean_bounds):
        '''Merge text'''
        merged_texts = []
        for merges in clean_bounds:
            merged_texts.append(
                ' '.join(list(self.new_dict.keys())[i] for i in merges))

        new_merged_texts = []
        for text in merged_texts:
            clean_txt = re.sub(self.char_pattern, '', text)
            new_merged_texts.append(clean_txt)

        return new_merged_texts


    def _extract_items_prices(self, merged_texts):
        '''Extract items and prices dict'''
        for text in merged_texts:
            # print(f'[DEBUG] Text: {text}')
            if re.match(self.item_price_pattern, text):
                # print(f'[DEBUG] Matched item: {text}')

                try:
                    price = float(
                        f'{float(re.search(self.price_re, text).group()):.2f}')
                except AttributeError:
                    price = 0.00

                try:
                    item = re.search(self.item_re, text).group()
                except AttributeError:
                    item = ' '.join([i.strip() for i in re.findall(self.item_re, text)
                                     if len(i.strip()) > 2])

                self.items_prices_dict['items'][item] = price


    def _clean_items_prices(self):
        '''
        Clean the items
            - Remove unwanted characters, multiple spaces, etc..
            - Remove skip words
            - Unionise the case across all items (title case)

        Clean the prices
            - remove very large numbers
        '''
        new_dict = {'items': {}, 'total': 0.00}

        for txt, price in self.items_prices_dict['items'].items():
            # Clean text
            clean_txt = re.sub(self.char_pattern, '', txt)
            clean_txt = re.sub(r'\s+', ' ', clean_txt)
            clean_txt = re.sub(r'\d{5,}', '', str(clean_txt))
            clean_price = re.sub(r'\d{5,}', '', str(price))
            clean_price = float(clean_price)

            # If a box contains any word that is in the skip words
            # Clean very large numbers
            if not set(clean_txt.lower().split(' ')).intersection(set(self.skip_words)) \
                    and len(clean_txt) > 1:
                new_dict['items'][clean_txt.strip().title()] = clean_price

        self.items_prices_dict['items'] = new_dict['items']


    def _calculate_total(self):
        '''Calculate total'''
        # Seek for a 'total' key
        for item, _ in self.items_prices_dict['items'].items():
            if set(item.lower().split(' ')).intersection(set(self.total_keys)):
                # print(f'[DEBUG] Matched total: {item}')
                self.items_prices_dict['total'] = self.items_prices_dict['items'][item]
                break

        # If 'total' is not found, calculate it
        if self.items_prices_dict['total'] == 0.00:
            total = float(
                f"{sum(self.items_prices_dict['items'].values()):.2f}")
            self.items_prices_dict['total'] = total


    def post_process(self, gcv_texts):
        '''
        Run the post-process pipeline on the GCV output

        Args:
            gcv_output (dict): output of GCV response

        Returns:
            items_prices_dict (dict): dict of items and their prices
        '''
        gcv_output = self._get_text_box(gcv_texts)

        self._format_dict(gcv_output)

        distance_matrix = self._calculate_distance_matrix()

        new_bounds = self._get_new_bounds(distance_matrix)

        clean_bounds = self._clean_bounding_boxes(new_bounds)
        # print(f'[DEBUG] clean_bounds: \n {clean_bounds}')

        merged_text = self._merge_text(clean_bounds)
        # print(f'[DEBUG] merged_text: \n {merged_text}')

        self._extract_items_prices(merged_text)
        # print(f'IPD before cleaning: {self.items_prices_dict}')

        # print('\n*****************************\n')

        self._clean_items_prices()

        self._calculate_total()
        # print(f'IPD after cleaning: {self.items_prices_dict}')

        return self.items_prices_dict


def main():
    '''Main'''
    PP = PostProc(img_height=1080)

    with open('./BKP/dict_sorted_2.json', 'r') as iff:
        gcv_output = json.load(iff)

    items_prices_dict = PP.post_process(gcv_output)
    print(f'IPD: {items_prices_dict}')


if __name__ == '__main__':
    main()


def gcv_post_proc(request):
    '''
    GCV Function main method.

    Args:
        request (json object): JSON object containing the GCV output

    Returns:
        response (JSON object)
        - In case of no errors: the items_prices_dict
        - In case of errors: the error message
    '''
    # JSON-ify the request
    request_json = request.get_json(silent=True)
    PP = PostProc(img_height=1080)

    # Run the PostProc algorithm
    try:
        items_prices_dict = PP.post_process(request_json)
        return jsonify({items_prices_dict})
    except Exception as e:
        return jsonify({"ERROR": e})
