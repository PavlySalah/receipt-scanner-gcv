'''
Preprocess a given image for text detection
'''
import os
import cv2
import numpy as np
from scipy.ndimage import interpolation as inter

def read_images(path):
    '''
    Read images from a given path

    Args:
        path (str): path of root folder to scan for images from

    Returns:
        images (list): list of images found in path
    '''
    images = []

    # Read image files
    for root, _, file_names in os.walk(path):
        for file_name in file_names:
            # Only read images (files that end in 'jpg' and 'png')
            if file_name.endswith(('jpg', 'png')):
                img_in = cv2.imread(root + '/' + file_name)
                images.append(img_in)

    print(f'[INFO] Found {len(images)} images in folder ./{path}')
    return images


def save_image(name, img):
    '''
    Save a given image.

    Args:
        img (numpy array): image to be shown
        name (str): name of the image
    '''
    out_path2 = 'Output_PP/'

    # Create the path if it doesn't exist
    if not os.path.exists(f'{out_path2}/'):
        os.mkdir(f'{out_path2}')

    # Save image in path
    cv2.imwrite(f'{out_path2}/{name}', img)


def show_image(name, img):
    '''
    Show a given image.

    Args:
        img (numpy array): image to be shown
        name (str): name of the windowe
    '''
    # Show image...
    cv2.imshow(name, img)

    if cv2.waitKey(0) == ord('q'):
        cv2.destroyAllWindows()


class PreProcessing():
    '''
    Preprocessing class
    '''
    def __init__(self):
        '''
        Initialize variables
        '''
        self.PP_counter = 0    # Counter for each pre-processing step


    def resize_image(self, img):
        '''
        Resize an image if it's too large

        Args:
            image (matrix): input image

        Returns:
            img_resized (matrix): iage after resizing
        '''
        print('[DEBUG] Resizing image')

        # Increment the pre-processing counter
        self.PP_counter += 1

        img_height, img_width, _ = img.shape
        # print(f'[DEBUG] Original image size: ({img_height}, {img_width})')

        # If the image size is large, resize it by half
        new_width = 2000

        # Preserve original image aspect ratio
        new_height = int(new_width * (img_height/img_width))

        # Resize image
        img_resized = cv2.resize(img, (new_width, new_height))
        print('[DEBUG] After resizing -- image size:', end=' ')
        print(f'({img_resized.shape[0]}, {img_resized.shape[1]})')

        return img_resized


    def greyscale_image(self, img):
        '''
        Convert image into greyscale

        Args:
            img (numpy array): input image

        Ret:
            img (numpy array): grey image
        '''
        print('[DEBUG] Converting image to grey scale')
        self.PP_counter += 1

        # Convert image to grey scale
        img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

        return img


    def threshold(self, img):
        '''
        Decrease effect of shadows

        Args:
            img (numpy array): input image

        Ret:
            img (numpy array): thresholdied image
        '''
        print('[DEBUG] Thresholding image')
        self.PP_counter += 1

        # Threshold image
        img = cv2.adaptiveThreshold(img, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C,\
            cv2.THRESH_BINARY, 255, 70)    # Best => 11, 4  => 71, 9  => 61, 9 => 131, 25

        return img


    def binarization(self, img):
        '''
        Convert image into black and white

        Args:
            img (numpy array): input image

        Ret:
            img (numpy array): binarized image
        '''
        print('[DEBUG] Binarizing image')
        self.PP_counter += 1

        # Binarize image
        _, img = cv2.threshold(img, 160, 255, cv2.THRESH_BINARY)
        return img


    def normalization(self, img):
        '''
        Normalize image to reduce effect of lighting

        Args:
            img (numpy array): input image

        Ret:
            img (numpy array): normalized image
        '''
        print('[DEBUG] Normalizing image')
        self.PP_counter += 1

        # Normalize image
        img = cv2.normalize(img, img, 0, 255, cv2.NORM_MINMAX)
        return img


    def opening(self, img):
        '''
        Reove diacritics

        Args:
            img (numpy array): input image

        Ret:
            img (numpy array): image with opening morphology
        '''
        print('[DEBUG] Applying opening morphology')
        self.PP_counter += 1

        # Opening morphology
        # If the background is black and the foreground (text) is white,
        # Opening removes separate excrences.
        # If the opposite, Opening fills small dilated regions
        kernel = np.ones((5, 5), np.uint8)
        img = cv2.morphologyEx(img, cv2.MORPH_OPEN, kernel)
        return img


    def closing(self, img):
        '''
        Fill in dialated characters

        Args:
            img (numpy array): input image

        Ret:
            img (numpy array): image with closing morphology
        '''
        print('[DEBUG] Applying closing morphology')
        self.PP_counter += 1

        # Closing morphology
        # If the background is black and the foreground (text) is white,
        # Closing fills small dilated regions.
        # If the opposite, Closing removes separate excrences.
        kernel = np.ones((2, 2), np.uint8)
        img = cv2.morphologyEx(img, cv2.MORPH_CLOSE, kernel)

        return img


    def dilate(self, img):
        '''
        It definitly does something

        Args:
            img (numpy array): input image

        Ret:
            img (numpy array): dilated image
        '''
        print('[DEBUG] Applying dilation')
        self.PP_counter += 1

        # Dilate image -- remove jointed excrences (in characters)
        kernel = np.ones((2, 2), np.uint8)
        img = cv2.dilate(img, kernel, iterations=1)
        return img


    def erode(self, img):
        '''
        Getting rid of excresences

        Args:
            img (numpy array): input image

        Ret:
            img (numpy array): eroded image
        '''
        print('[DEBUG] Applying erosion')
        self.PP_counter += 1

        # Erode characters
        kernel = np.ones((1, 1), np.uint8)
        img = cv2.erode(img, kernel, iterations=1)

        return img


    def _find_score(self, arr, angle):
        data = inter.rotate(arr, angle, reshape=False, order=0)
        hist = np.sum(data, axis=1)
        score = np.sum((hist[1:] - hist[:-1]) ** 2)
        return hist, score


    def canny_edge_detector(self, img):
        '''
        Emphasize edges

        Args:
            img (numpy array): input image

        Ret:
            img (numpy array): image with emphasized edges
        '''
        print('[DEBUG] Detecting significat edges')
        self.PP_counter += 1

        # Canny edge detector
        img = cv2.Canny(img, 80, 250)

        return img


    def smoothing(self, img):
        '''
        Remove noise

        Args:
            img (numpy array): input image

        Ret:
            img (numpy array): smooth image
        '''
        print('[DEBUG] Smoothing image')
        self.PP_counter += 1

        if img.shape[0] < 800:
            print('[DEBUG] NO SHARPENING WAS APPLIED DUE TO LOW RES')
            return img

        # Smooth image by applying a slight blur
        img = cv2.GaussianBlur(img, (5, 5), cv2.BORDER_DEFAULT)
        return img


    def sharpen(self, img, img_smooth):
        '''
        Sharpen image (increase contrast)

        Args:
            img (numpy array): input image
            img_smooth (numpy array): blurred image

        Ret:
            img (numpy array): sharpened image
        '''
        print('[DEBUG] Sharpening image')
        self.PP_counter += 1

        # Sharpen image by 'adding' the oriinal image to the smooth image
        img = cv2.addWeighted(img, 1.5, img_smooth, -0.5, 2)    # 2 -1
        return img


    def equalize_hist(self, img):
        '''
        Equalize image histogram

        Args:
            img (numpy array): input image

        Ret:
            img (numpy array): smooth image
        '''
        print('[DEBUG] Equalizing image')
        self.PP_counter += 1

        # Equilize image histogram
        img = cv2.equalizeHist(img)
        return img


    def high_dynamic_range(self, img):
        '''
        Cnvert an image to HDR

        Args:
            img (numpy array): input image

        Ret:
            img (numpy array): HDR image
        '''
        print('[DEBUG] HDRing image')
        self.PP_counter += 1

        # Enhance image details (HDR)
        img = cv2.detailEnhance(img, sigma_s=200, sigma_r=0.25)    # 200 .15

        # Smooth everything except edges (emphasize edges) 60 25
        img = cv2.edgePreservingFilter(img, flags=1, sigma_s=100, sigma_r=0.25)    # 10 .45

        return img


    def upscale(self, img, scale_factor):
        '''Upscale'''
        print('[DEBUG] Upscaling image')
        # Create an SR object - only function that differs from c++ code
        sr = dnn_superres.DnnSuperResImpl_create()

        # Read the desired model
        sr.readModel("data/EDSR_x4.pb")

        # Set the desired model and scale to get correct pre- and post-processing
        sr.setModel("edsr", scale_factor)

        # Upscale the image
        result = sr.upsample(img)

        return result


    def increase_contrast(self, img):
        '''What the method  name says'''
        print('[DEBUG] increasing image contrast')
        self.PP_counter += 1

        lookUpTable = np.empty((1, 256), np.uint8)

        gamma = 1.5    # 2.2

        for i in range(256):
            lookUpTable[0, i] = np.clip(pow(i / 255.0, gamma) * 255.0, 0, 255)

        out_img = cv2.LUT(img, lookUpTable)
        return out_img


    def invert_img(self, img):
        '''What the method  name says'''
        print('[DEBUG] inverting image')
        self.PP_counter += 1

        out_img = 255 - img

        return out_img


    def pog(self, img):
        '''Detail Enhancement'''
        print('[DEBUG] PogU')

        self.PP_counter += 1

        out_img = cv2.detailEnhance(img, sigma_s=200, sigma_r=0.25)

        return out_img


    def preprocess(self, img, img_num):
        '''
        Main function. Perform all above steps

        Args:
            img (numpy array): input image

        Ret:
            img (numpy array): preprocessed image
        '''
        img = self.upscale(img, 4)
        # show_image('High contrast', img)
        save_image(f'img_{img_num}/{self.PP_counter}_Upscale.jpg', img)

        img = self.increase_contrast(img)
        # show_image('High contrast', img)
        save_image(f'img_{img_num}/{self.PP_counter}_HighCon_image.jpg', img)

        img = self.pog(img)
        # show_image('High contrast', img)
        save_image(f'img_{img_num}/{self.PP_counter}_Pog.jpg', img)

        img = self.greyscale_image(img)
        # show_image('Grey image', img)
        save_image(f'img_{img_num}/{self.PP_counter}_Grey_image.jpg', img)

        img = self.equalize_hist(img)
        # show_image('Equalized image', img)
        save_image(f'img_{img_num}/{self.PP_counter}_equi_image.jpg', img)

        img = self.threshold(img)
        # show_image('Thresholded image', img_thresh)
        save_image(f'img_{img_num}/{self.PP_counter}_Thresholded_image.jpg', img)

        img = self.closing(img)
        # show_image('Closing morphology image', img)
        save_image(f'img_{img_num}/{self.PP_counter}_Closed_image.jpg', img)

        return img


def check_one_image(img_path, out_path):
    '''
    Check one image

    Args:
        img_path (str): input path of the images
        output_path (str): output path
    '''
    PP = PreProcessing()

    image = cv2.imread(img_path)

    if not os.path.exists(f'{out_path}/img_0'):
        os.mkdir(f'{out_path}/img_0')

    image = PP.preprocess(image, 0)


def check_sample_images(n, out_path):
    '''
    Check N random images and save the pre-processed images

    Args:
        n (int): number of images to check
        output_path (str): output path
    '''
    rand_indices = set(np.random.randint(0, 9, n))

    images = read_images('data/ME')

    my_images = [images[i] for i in rand_indices]

    for i, image in zip(rand_indices, my_images):
        PP = PreProcessing()
        if not os.path.exists(f'{out_path}/img_{i}'):
            os.mkdir(f'{out_path}/img_{i}')

        image = PP.preprocess(image, i)


def check_all_images(out_path):
    '''
    Check all images

    Args:
        out_path (str): Output path to save the pre-processed images
    '''
    my_images = read_images('data/ME')

    for i, image in enumerate(my_images):
        PP = PreProcessing()
        if not os.path.exists(f'{out_path}/img_{i}'):
            os.mkdir(f'{out_path}/img_{i}')

        image = PP.preprocess(image, i)


def main():
    '''main'''
    out_path = 'Output_PP'

    # check_one_image('data/ME/1.jpg', out_path)
    check_sample_images(5, out_path)
    # check_all_images(out_path)


if __name__ == '__main__':
    from cv2 import dnn_superres
    main()
